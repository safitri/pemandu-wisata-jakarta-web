<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ordered_by' => 'required|max:255',
            'citizenship_id' => 'required|digits:16',
            'adult_ticket_count' => 'required|numeric',
            'children_ticket_count' => 'required|numeric',
            'destination_name' => 'required|max:255',
            'check_in_date' => 'required|date',
            'email' => 'required|email|max:255',
            'phone_number' => 'required|max:127'
        ];
    }
}
