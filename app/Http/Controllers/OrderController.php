<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\FindOrderRequest;
use App\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except('create', 'find', 'welcome');
    }

    public function displayRecent(Request $request)
    {
        $orders = Order::query()
            ->where('paid_at', '=', null)
            ->where('canceled_at', '=', null)
            ->paginate(20);
        return view('order.index', [
            'title' => 'Pemesanan Baru',
            'show_paid_button' => true,
            'orders' => $orders,
        ]);
    }

    public function displayPaid(Request $request)
    {
        $orders = Order::query()
            ->where('paid_at', '!=', null)
            ->where('canceled_at', '=', null)
            ->paginate(20);
        return view('order.index', [
            'title' => 'Pemesanan Diterima',
            'orders' => $orders,
            'show_paid_time' => true,
        ]);
    }

    public function displayCanceled(Request $request)
    {
        $orders = Order::query()
            ->where('canceled_at', '!=', null)
            ->paginate(20);
        return view('order.index', [
            'title' => 'Pemesanan Dibatalkan',
            'orders' => $orders,
        ]);
    }

    public function paid(Order $order)
    {
        $order->paid_at = Carbon::now()->toDateTimeString();
        $order->save();
        return redirect()->back();
    }

    public function cancel(Order $order)
    {
        $order->canceled_at = Carbon::now()->toDateTimeString();
        $order->save();
        return redirect()->back();
    }

    public function create(CreateOrderRequest $request)
    {
        $order = new Order();
        $order->fill($request->all());
        $order->save();
        return $order;
    }

    public function find(FindOrderRequest $request)
    {
        $orders = Order::query()
            ->where('ordered_by', '=', $request->get('ordered_by'))
            ->simplePaginate(20);
        return $orders;
    }

    public function welcome(Order $order)
    {
        return view('order.welcome', ['order' => $order, 'hide_menu' => true]);
    }

}
