create table orders
(
    id                    bigint unsigned auto_increment
        primary key,
    ordered_by            varchar(255) not null,
    citizenship_id        varchar(255) not null,
    adult_ticket_count    int          not null,
    children_ticket_count int          not null,
    destination_name      varchar(255) not null,
    check_in_date         date         not null,
    email                 varchar(255) not null,
    phone_number          varchar(255) not null,
    paid_at               datetime     null,
    redeemed_at           datetime     null,
    canceled_at           datetime     null,
    created_at            timestamp    null,
    updated_at            timestamp    null
)
    collate = utf8mb4_unicode_ci;

