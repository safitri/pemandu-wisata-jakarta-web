<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('order')->group(function () {

    Route::get('recent', 'OrderController@displayRecent')->name('web.order.recent');

    Route::get('paid', 'OrderController@displayPaid')->name('web.order.paid');

    Route::get('canceled', 'OrderController@displayCanceled')->name('web.order.canceled');

    Route::post('paid/{order}', 'OrderController@paid')->name('web.order.paid.action');

    Route::post('cancel/{order}', 'OrderController@cancel')->name('web.order.cancel.action');

    Route::get('welcome/{order}', 'OrderController@welcome')->name('web.order.welcome');

});
