@extends('layouts.app')

@section('container')
    <div class="container-fluid">
        <div class="row justify-content-center">

            <div class="col-xl-2 col-md-3">
                <div class="list-group">
                    <a href="{{ route('home') }}" class="list-group-item list-group-item-action @if(request()->route()->getName() == 'home') active @endif">
                        Dashboard
                    </a>
                    <a href="{{ route('web.order.recent') }}" class="list-group-item list-group-item-action @if(request()->route()->getName() == 'web.order.recent') active @endif">
                        Pemesanan Baru
                    </a>
                    <a href="{{ route('web.order.paid') }}" class="list-group-item list-group-item-action @if(request()->route()->getName() == 'web.order.paid') active @endif">
                        Pesanan Terbayar
                    </a>
                    <a href="{{ route('web.order.canceled') }}" class="list-group-item list-group-item-action @if(request()->route()->getName() == 'web.order.canceled') active @endif">
                        Pesanan Dibatalkan
                    </a>
                </div>
            </div>

            <div class="col-xl-10 col-md-9">
                @yield('content')
            </div>
        </div>
    </div>
@endsection
