@extends('layouts.app')

@section('container')
    <div class="container-fluid">
        <div class="justify-content-center">

            <div class="card">

                <div class="card-body">

                    <h4 class="text-center">Selamat Datang, {{ $order->ordered_by }} di {{ $order->destination_name }}!</h4>

                </div>

            </div>

        </div>
    </div>
@endsection
