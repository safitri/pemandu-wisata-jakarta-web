@extends('layouts.dashboard')

@section('content')
    <div class="card">
        <div class="card-header">{{ $title }}</div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <table class="table table-hover table-bordered">
                <tr>
                    <th rowspan="2" class="align-middle text-center">#</th>
                    <th rowspan="2" class="align-middle text-center">Pemesan</th>
                    <th colspan="2" class="align-middle text-center">Jumlah Tiket</th>
                    <th rowspan="2" class="align-middle text-center">Tempat Wisata</th>
                    <th rowspan="2" class="align-middle text-center">Tanggal Check In</th>
                    <th rowspan="2" class="align-middle text-center">Kontak</th>
                    <th rowspan="2" class="align-middle text-center">Waktu Pemesanan</th>
                    @if(isset($show_paid_time) && $show_paid_time)
                    <th rowspan="2" class="align-middle text-center">Waktu Konfirmasi Pembayaran</th>
                    @endif
                    @if(isset($show_paid_button) && $show_paid_button)
                    <th rowspan="2" class="align-middle text-center">Aksi</th>
                    @endif
                </tr>
                <tr>
                    <th class="align-middle text-center">Dewasa</th>
                    <th class="align-middle text-center">Anak</th>
                </tr>
            @foreach($orders as $order)
                <tr>
                    <td class="align-middle text-center">{{ $order->id }}</td>
                    <td class="align-middle text-center">{{ $order->ordered_by }}<br/><small>{{ $order->citizenship_id }}</small></td>
                    <td class="align-middle text-center">{{ $order->adult_ticket_count }}</td>
                    <td class="align-middle text-center">{{ $order->children_ticket_count }}</td>
                    <td class="align-middle text-center">{{ $order->destination_name }}</td>
                    <td class="align-middle text-center">{{ $order->check_in_date }}</td>
                    <td class="align-middle text-center">{{ $order->phone_number }}<br/><small>{{ $order->email }}</small></td>
                    <td class="align-middle text-center">{{ $order->created_at }}</td>
                    @if(isset($show_paid_time) && $show_paid_time)
                    <th class="align-middle text-center">{{ $order->paid_at }}</th>
                    @endif
                    @if(isset($show_paid_button) && $show_paid_button)
                    <th class="align-middle text-center">
                        <form action="{{ route('web.order.paid.action', $order->id) }}" method="post">
                            @csrf
                            <button type="submit" class="btn btn-primary">Konfirmasi Pembayaran</button>
                        </form>
                        <form action="{{ route('web.order.cancel.action', $order->id) }}" method="post" style="margin-top: 8px">
                            @csrf
                            <button type="submit" class="btn btn-danger">Batalkan Pesanan</button>
                        </form>
                    </th>
                    @endif
                </tr>
            @endforeach
            </table>
        </div>
    </div>
@endsection
