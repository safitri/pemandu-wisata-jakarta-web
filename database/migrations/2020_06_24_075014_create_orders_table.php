<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('ordered_by');
            $table->string('citizenship_id');
            $table->integer('adult_ticket_count');
            $table->integer('children_ticket_count');
            $table->string('destination_name');
            $table->date('check_in_date');
            $table->string('email');
            $table->string('phone_number');
            $table->dateTime('paid_at')->nullable();
            $table->dateTime('redeemed_at')->nullable();
            $table->dateTime('canceled_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
